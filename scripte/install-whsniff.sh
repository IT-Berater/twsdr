#!/bin/bash

#
# Thomas Wenzlafff
#

cd ~
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install libusb-1.0-0-dev tshark

# wir holen uns das whsniff von GitHub und compilieren es
curl -L https://github.com/homewsn/whsniff/archive/v1.1.tar.gz | tar zx
cd whsniff-1.1
make
sudo make install
