#!/bin/bash

#
# Thomas Wenzlafff
# Instalation der rtl_map Anwendung

# das rtl_map Git Repo clonen
git clone https://github.com/KeyLo99/rtl_map.git

# jetzt in das rtl_map Verzeichnis gehen
cd rtl_map/
# ein build Verzeichnis erstellen und dort rein wechseln
mkdir build
cd build/
# Compile starten
cmake ../
# Make
make
# Install
sudo make install

# in das Homeverzeichnis gehen
cd ~
# Testen, Ausgeben der Programm Parameter
rtl_map -h