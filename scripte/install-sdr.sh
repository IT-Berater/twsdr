#!/bin/bash

#
# Thomas Wenzlaff 
# Erstellt SDR Anwendungen
#

#sudo apt-get install
#sudo apt-get update

# Libs für den DVB-Empfänger installieren
sudo apt-get install git git-core cmake libusb-1.0-0-dev build-essential
git clone git://git.osmocom.org/rtl-sdr.git
cd rtl-sdr/
mkdir build
cd build/
sudo cmake ../ -DINSTALL_UDEV_RULES=ON -DDETACH_KERNEL_DRIVER=ON
sudo make 	
sudo make install
sudo ldconfig
cd ~
sudo cp ./rtl-sdr/rtl-sdr.rules /etc/udev/rules.d/

echo "Nun ein reboot ausführen und rtl_test -t eingeben"

