#!/bin/bash

#
# Thomas Wenzlafff
#
# evl. auch noch nötig

# einige nötige Abhängikeiten installieren
sudo apt-get install libtool libusb-1.0.0-dev librtlsdr-dev rtl-sdr
 
# das rtl_433 Git Repo clonen
git clone https://github.com/merbanan/rtl_433.git
 
# jetzt in das rtl_433 Verzeichnis gehen
cd rtl_433/
# ein build Verzeichnis erstellen und dort rein wechseln
mkdir build
cd build/
# Compile starten
cmake ../
# Make
make
# Install
sudo make install
 
# in das Homeverzeichnis gehen
cd ~
# Testen, Ausgeben der Programm Parameter
rtl_433 -h
