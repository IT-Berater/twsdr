#!/bin/bash

#
# Thomas Wenzlaff
#
# Macht ein update des rtl_433 Programm
#

cd ~

# jetzt in das rtl_433 Verzeichnis gehen
cd rtl_433/

# das rtl_433 Git Repo updaten
git pull https://github.com/merbanan/rtl_433.git

cd build/

# Compile starten
cmake ../

# Make
make

# Install
sudo make install

# in das Homeverzeichnis gehen
cd ~

# Testen, Ausgeben der Programm Parameter
rtl_433 -h