#!/bin/bash

#
# Thomas Wenzlafff
#

# zuerst erstellen wir uns ein Verzeichnis
cd ~
mkdir acars
cd acars
# dann clonen wir das Repo von GitHub
git clone https://github.com/TLeconte/acarsdec.git
# installieren noch ein paar benötigte Abhängikeiten
sudo apt-get install libsndfile1-dev
sudo apt-get install libasound2-dev
# evl. auch noch nötig
#sudo apt-get install librtlsdr
sudo ldconfig
# wechseln in das Verzeichnis
cd acarsdec
mkdir build   
cd build/
cmake .. -Drtl=ON
make
sudo make install
echo "Installation abgeschlossen. Testausgabe acarsdec -h"
cd ~
# Test Hilfe ausgeben
acarsdec -h

